longString = ""
arr1 = [] # array of proper data
 

chunkOutputCode = "
<div class='outputChunk'>
	<div class='outputChunkName'></div>
	<textarea class='outputChunkDomain' disabled></textarea><br>
	<textarea class='outputChunkLinks' disabled></textarea><br>
	<textarea class='inputCode'></textarea>
</div>
"


jQuery("input#fileInput").change ->
	$("#second").css({ opacity: 1 });


String::replaceAt = (index, character) ->
	@substr(0, index) + character + @substr(index + character.length)

viewDomains = ->
	$(".parseResults").modal()
	$("#longString").html("")
	
	$("#longString").html("<pre>"+longString+"</pre>")

viewLinks = ->
	$(".parseResults2").modal()
	$("#array").html("")

	for i of arr1
		$("#array").append(arr1[i])
		$("#array").append("<br>")

syncScroll = ->
	$divs = $(".final, .posFinal")
	sync = (e) ->
		$other = $divs.not(this).off("scroll")
		other = $other.get(0)
		percentage = @scrollTop / (@scrollHeight - @offsetHeight)
		other.scrollTop = percentage * (other.scrollHeight - other.offsetHeight)
		setTimeout (->
			$other.on "scroll", sync
		), 10

	$divs.on "scroll", sync

parseCSV = ->
	data = undefined

	arr0 = [] # array of unproper data
	arr2 = [] # non-unique domains
	arr3 = [] # unique domains

	file = $("#fileInput")[0].files[0]
	Papa.parse file,
		header: true
		dynamicTyping: true
		complete: (results) ->
			data = results

			data["data"].forEach (arrayItem) ->  # get data from csv file to array
				arr0.push arrayItem

			for key of arr0 # get proper data to 
				obj = arr0[key]
				for prop of obj
					arr1.push obj[prop]	if obj.hasOwnProperty(prop)

			if arr1[arr1.length - 1] is "" # check! # delete last, emply element
				arr1.pop()

			for i of arr1 # delete http://*
				try
					arr1[i] = arr1[i].replace("http://", "")

			for i of arr1 # delete www.*
				try
					arr1[i] = arr1[i].replace("www.", "")

			for i of arr1 # delete */
				try
					if arr1[i].charAt(arr1[i].length - 1) == "/"
						arr1[i] = arr1[i].substring(0, arr1[i].length - 1)

			arr2 = arr1.slice() # copy array by value for domains

			for i of arr2 # get domains
				try
					for j of arr2[i]
						if arr2[i].charAt(j) == "/"
							arr2[i] = arr2[i].substring(0, j)
							break

			arr3 = arr2.filter((item, pos) -> # got unique domains
				arr2.indexOf(item) is pos
			)

			for i of arr3  # make long, pipe-divided string
				longString += arr3[i]
				longString += "|"
			longString = longString.substring(0, longString.length - 1)



			# console.log longString
			# console.log arr1
			$("#viewDomains").fadeIn()
			$("#viewLinks").fadeIn()
			setTimeout (->
				$("#third").css opacity: 1
			), 400


chunks = []
chunkDomains = []
divideResults = ->
	
	part = $("#divideInput").val()
	part2 = parseInt(part)
	a = 0


	while a < arr1.length
		if a+part2 < arr1.length
			chunks.push(arr1.slice(a,a+part2))
			a+=part2
		else
			chunks.push(arr1.slice(a,arr1.length))
			break

	chunkDomainsTemp = []
	numberOfLines = 0
	lastPos = 0


	i = 0
	while i < chunks.length
		j = 0
		# console.log "i="+i
		# console.log chunks[i].length
		while j < chunks[i].length
			try
				for k of chunks[i][j]  # get domains
					if chunks[i][j].charAt(k) == "/"
						chunkDomainsTemp.push(chunks[i][j].substring(0, k))	
						break
			chunks[i][j] = "(pontorez | url:"+chunks[i][j]+"* | url:www."+chunks[i][j]+"*)"
			j++


		chunkDomainsTemp2 = chunkDomainsTemp.filter((item, pos) -> # got unique domains
				chunkDomainsTemp.indexOf(item) is pos
		)

		chunkDomains.push(chunkDomainsTemp2)
		chunkDomainsTemp = []
		chunkDomainsTemp2 = []
		i++
			

	# console.log chunks
	# console.log chunkDomains


$(window).load ->
	$("#second").css({ opacity: 0.2 });
	$("#third").css({ opacity: 0.2 });

$("#parseButton").on "click", ->
	parseCSV()
	$(this).prop("disabled",true)
	$(this).css({ opacity: 0.2 });


$("#divideButton").on "click", ->
	divideResults()
	$(this).prop("disabled",true)
	$("#divideInput").prop("disabled",true)
	$(this).css({ opacity: 0.2 });

	$("body").append("<div class='outputContainerWrapper'></div>")
	$(".outputContainerWrapper").append("<div class='outputContainer'></div>")
	$(".outputContainer").append("<div class='step4'>4) Get positions</div><br>")
	i=0
	while i < chunkDomains.length
		$(".outputContainer").append $(chunkOutputCode).attr("id","output"+i)
		ii = i+1
		$("#output"+i).children(".outputChunkName").html("#"+ii)
		$("#output"+i).children(".outputChunkDomain").val(chunkDomains[i].join("|"))
		$("#output"+i).children(".outputChunkLinks").val(chunks[i].join("\n"))
		$("#output"+i).children(".chunkResult").attr("id","selectFile"+i)
		$("#output"+i).children(".chunkResult").attr("name","file"+i)
		$("#output"+i).children(".inputCode").attr("id","code"+i)
		i++
	$(".outputContainer").append("<br><br><a target='_blank' href='pos.php'>insert data here</a><br>")
	$(".outputContainer").append("<br><button id='filesReady'>done</button><br>")







$(document).on "click", "#filesReady", ->
	$(this).css({ opacity: 0.2 });
	$(this).prop("disabled",true)

	code = ""
	str1 = ''
	str2 = ''
	visualArray = []

	$(".inputCode").each (i) ->
		if $(this).val() != ""
			code = $(this).val()
			codeArray = code.split("\n");
			resultArray = []
			for j of codeArray
				hg = codeArray[j].lastIndexOf("	")
				if (!isNaN(parseInt(codeArray[j][hg+1])))
					resultArray.push(codeArray[j])
			resultString = resultArray.join("\n");
			posArray = []
			urlArray = []


			for j of resultArray
				aa = resultArray[j].split("	")
				bb = aa[1].substring(16)
				cc = bb.indexOf("url:www.")
				dd = bb.substring(0,cc-4)
				posArray.push(aa[2])
				urlArray.push(dd)

			visualArray = []
			for j of posArray
				visualArray.push(posArray[j]+"	"+urlArray[j])

			
	visualArray.sort()
	for j of visualArray
		aa = visualArray[j].split("	")
		str1 += aa[0]+"\n"
		str2 += aa[1]+"\n"

	$(".outputContainerWrapper").append("<div class='outputContainer2'></div>")
	$(".outputContainer2").append("<h2>results:</h2>")
	$(".outputContainer2").append("<textarea class='posFinal' disabled>"+str1+"</textarea>")
	$(".outputContainer2").append("<textarea class='final' disabled>"+str2+"</textarea>")
	setTimeout (->
		syncScroll()
	), 100





