﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru-RU">
<head profile="http://gmpg.org/xfn/11">
	<title>chelink | get positions</title>

	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			$(".accordion h3:first").addClass("active");
			$(".accordion ol").hide();

			$(".accordion h3").click(function(){
				$(this).next("ol").slideToggle("slow")
				.siblings("ol:visible").slideUp("slow");
				$(this).toggleClass("active");
				$(this).siblings("h3").removeClass("active");
			});

		});
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	<link rel="icon" href="favicon.ico">
</head>

<body>
	<div class="accordion">
<?php

	$global_pages_count = 100;
	$i = 1;
	$last_url = '';
	$concurents = '';
	
	function my_download($url,$post){
		
		$ret = false;
		
		if( function_exists('curl_init') ){
			if( $curl = curl_init() ){
				
				if( !curl_setopt($curl,CURLOPT_URL,$url) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_RETURNTRANSFER,true) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,30) ) return $ret;
			
				if( !curl_setopt($curl,CURLOPT_HEADER,false) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_POST,true) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_POSTFIELDS,$post) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_ENCODING,"gzip,deflate") ) return $ret;
				
				$ret = curl_exec($curl);
				
				curl_close($curl);
			}
		}
		else{
			$u = parse_url($url);
			
			if( $fp = fsockopen($u['host'],!empty($u['port']) ? $u['port'] : 80 ) ){
				
			    $headers = 'POST '.  $u['path'] .' HTTP/1.0'. "\r\n";
			    $headers .= 'Host: '. $u['host'] ."\r\n";
				$headers .= 'Content-type: application/xml' . "\r\n";
				$headers .= 'Content-length: ' . strlen($post) . "\r\n";
			    $headers .= 'Connection: Close' . "\r\n\r\n";
				$headers .= $post;
				
			    fwrite($fp, $headers);
			    $ret = '';
					
				while( !feof($fp) ){
					$ret .= fgets($fp,1024);
				}
				
				$ret = substr($ret,strpos($ret,"\r\n\r\n") + 4);
				
				fclose($fp);
			}
		}
		
		return $ret;
	}
	
	function my_line($key,$message,$concurents = ''){
		global $i,$hostname;
		
		if( $i%2 == 0 )
			$color = '#F8F8F8';
		else
			$color = '#F0F0F0';
			
		echo "<tr bgcolor=\"$color\"><td valign=\"top\">$i</td><td class=\"k\" valign=\"top\">$key</td><td class=\"p\" valign=\"top\">$message</td></tr>";
		
		$i++;
	}
	
	function my_error($text){
		echo '<div id="error">' . $text . '</div>';
	}

	$functions_error = false;
	if( !class_exists('SimpleXMLElement') ){
		my_error('Ваш PHP не поддерживает функции XML. PHP4 устарел, обновитесь до PHP5.');
		$functions_error = true;
	}
	if( !function_exists('curl_init') && !function_exists('fsockopen') ){
		my_error('Ваш PHP не поддерживает функции для работы с http протоколом (curl или сокеты).');
		$functions_error = true;
	}
	
	if( isset($_POST["send"]) && isset($_POST["keys"]) && isset($_POST["hostname"]) && !empty($_POST["keys"]) && !empty($_POST["hostname"]) && !$functions_error ){
		
		$keys = explode("\r\n",$_POST["keys"]);
		$hostname = htmlspecialchars($_POST["hostname"]);			
		$host = preg_replace("[^http://|www\.]",'',$_POST["hostname"]);
		
		if( !empty($keys) ){
		
			echo '<table style="border: 1px dotted #9C9C9C;" align="center" cellpadding="4"><tr bgcolor="#DBDBDB"><td>&nbsp;</td><td><strong>url</strong></td><td><strong>position</strong></td></tr>';
		
			foreach( $keys as $key ){
			
				if( empty($key) ) continue;
			
				$key = htmlspecialchars($key);
				
				$page = $found = 0;
				$pages = round($global_pages_count/100);
				$error = false;
				$exit = $_SERVER["REQUEST_METHOD"] != 'POST';
				$concurents = '';
				
				while (!$exit && $page < $pages && $host){

				    // XML запрос
				    $xml_code = "<?xml version='1.0' encoding='utf-8'?>" .
						"<request>" .
							"<query>$key</query>" .
							"<page>$page</page>" .
				    		"<maxpassages>0</maxpassages>" .
				    		"<groupings>" .
				        		"<groupby attr='d' mode='deep' groups-on-page='100' docs-in-group='1' curcateg='-1'/>" .
				    		"</groupings>" .
						"</request>";

		    		if( $response = my_download('http://xmlsearch.yandex.ru/xmlsearch?user=scrapi&key=03.16129070:35c97c86c25bad3e7f12c75c72fb3040',$xml_code) ){
					
				        $xmldoc = new SimpleXMLElement($response);
		        		$xmlresponce = $xmldoc->response;
						
				        if ($xmlresponce->error) {
							my_error('Возникла следующая ошибка: ' . $xmlresponce->error);
				            $exit  = 1;
				            $error = true;
				            break;
				        }
						
				        $pos = 1;
		        		$nodes = $xmldoc->xpath('/yandexsearch/response/results/grouping/group/doc/url');
		        		
				        foreach($nodes as $node) {
				        	
							$found++;
							
							$url = parse_url($node);
							
				            // если URL начинается с имени хоста, выходим из цикла
				            if( preg_match("/^http:\/\/(www\.)?$host/i", $node) ) {
								$concurents .= '<li><strong>'. $url['host'] .'<strong></li>';
								$last_url = $node;
				                $exit = 1;
				                break;
				            }
							else{
								$concurents .= '<li><a href="'. $node .'">'. $url['host'] .'</a></li>';
							}
				            $pos++;
				        }
				        
		        		$page++;
				    }else{
						my_line($key,'Не удалось сделать запрос на сервер Яндекса');
				        $exit = 1;
				    }
				}
				
				if( !$error ) {
				    if( $found < $global_pages_count ) {
						my_line('<a href="' . $last_url .'">' . $key . '</a>',$found,$concurents);
				    }
				    else{
						my_line($key,'-');
				    }
				}		
			} // foreach( $keys as $key ){
			
			echo '</table>';
		} // if( !empty($keys) ){
	}

?>
	</div>
	
	<br /><br />
	<form method="post">
		<table cellpadding="4" style="font-size: 18px;" align="center">
		<tr><td></td><td><a target='_blank' href="./index.html">get back</a></td></tr>
			<tr>
				<td>domains</td><td><textarea type="text" style="width: 100%" cols="40" rows="7" name="hostname" value="" /></textarea></td>
			</tr>
			<tr>
				<td>urls</td><td><textarea style="width: 100%" cols="40" rows="7" name="keys"></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td style="text-align:center"><input type="submit" name="send" value="go for it!" /></td>
			</tr>
		</table>
	</form>

	
	<!-- <div id="footer">&copy; 2008&nbsp;<a href="http://www.samborsky.com/">Самборский Евгений</a>&nbsp;|&nbsp;<a href="http://www.samborsky.com/yandex-positions/">Последняя версия утилиты</a></div> -->

	
</body>

</html>