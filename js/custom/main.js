var arr1, chunkDomains, chunkOutputCode, chunks, divideResults, longString, parseCSV, syncScroll, viewDomains, viewLinks;

longString = "";

arr1 = [];

chunkOutputCode = "<div class='outputChunk'> <div class='outputChunkName'></div> <textarea class='outputChunkDomain' disabled></textarea><br> <textarea class='outputChunkLinks' disabled></textarea><br> <textarea class='inputCode'></textarea> </div>";

jQuery("input#fileInput").change(function() {
  return $("#second").css({
    opacity: 1
  });
});

String.prototype.replaceAt = function(index, character) {
  return this.substr(0, index) + character + this.substr(index + character.length);
};

viewDomains = function() {
  $(".parseResults").modal();
  $("#longString").html("");
  return $("#longString").html("<pre>" + longString + "</pre>");
};

viewLinks = function() {
  var i, _results;
  $(".parseResults2").modal();
  $("#array").html("");
  _results = [];
  for (i in arr1) {
    $("#array").append(arr1[i]);
    _results.push($("#array").append("<br>"));
  }
  return _results;
};

syncScroll = function() {
  var $divs, sync;
  $divs = $(".final, .posFinal");
  sync = function(e) {
    var $other, other, percentage;
    $other = $divs.not(this).off("scroll");
    other = $other.get(0);
    percentage = this.scrollTop / (this.scrollHeight - this.offsetHeight);
    other.scrollTop = percentage * (other.scrollHeight - other.offsetHeight);
    return setTimeout((function() {
      return $other.on("scroll", sync);
    }), 10);
  };
  return $divs.on("scroll", sync);
};

parseCSV = function() {
  var arr0, arr2, arr3, data, file;
  data = void 0;
  arr0 = [];
  arr2 = [];
  arr3 = [];
  file = $("#fileInput")[0].files[0];
  return Papa.parse(file, {
    header: true,
    dynamicTyping: true,
    complete: function(results) {
      var i, j, key, obj, prop;
      data = results;
      data["data"].forEach(function(arrayItem) {
        return arr0.push(arrayItem);
      });
      for (key in arr0) {
        obj = arr0[key];
        for (prop in obj) {
          if (obj.hasOwnProperty(prop)) {
            arr1.push(obj[prop]);
          }
        }
      }
      if (arr1[arr1.length - 1] === "") {
        arr1.pop();
      }
      for (i in arr1) {
        try {
          arr1[i] = arr1[i].replace("http://", "");
        } catch (_error) {}
      }
      for (i in arr1) {
        try {
          arr1[i] = arr1[i].replace("www.", "");
        } catch (_error) {}
      }
      for (i in arr1) {
        try {
          if (arr1[i].charAt(arr1[i].length - 1) === "/") {
            arr1[i] = arr1[i].substring(0, arr1[i].length - 1);
          }
        } catch (_error) {}
      }
      arr2 = arr1.slice();
      for (i in arr2) {
        try {
          for (j in arr2[i]) {
            if (arr2[i].charAt(j) === "/") {
              arr2[i] = arr2[i].substring(0, j);
              break;
            }
          }
        } catch (_error) {}
      }
      arr3 = arr2.filter(function(item, pos) {
        return arr2.indexOf(item) === pos;
      });
      for (i in arr3) {
        longString += arr3[i];
        longString += "|";
      }
      longString = longString.substring(0, longString.length - 1);
      $("#viewDomains").fadeIn();
      $("#viewLinks").fadeIn();
      return setTimeout((function() {
        return $("#third").css({
          opacity: 1
        });
      }), 400);
    }
  });
};

chunks = [];

chunkDomains = [];

divideResults = function() {
  var a, chunkDomainsTemp, chunkDomainsTemp2, i, j, k, lastPos, numberOfLines, part, part2, _results;
  part = $("#divideInput").val();
  part2 = parseInt(part);
  a = 0;
  while (a < arr1.length) {
    if (a + part2 < arr1.length) {
      chunks.push(arr1.slice(a, a + part2));
      a += part2;
    } else {
      chunks.push(arr1.slice(a, arr1.length));
      break;
    }
  }
  chunkDomainsTemp = [];
  numberOfLines = 0;
  lastPos = 0;
  i = 0;
  _results = [];
  while (i < chunks.length) {
    j = 0;
    while (j < chunks[i].length) {
      try {
        for (k in chunks[i][j]) {
          if (chunks[i][j].charAt(k) === "/") {
            chunkDomainsTemp.push(chunks[i][j].substring(0, k));
            break;
          }
        }
      } catch (_error) {}
      chunks[i][j] = "(pontorez | url:" + chunks[i][j] + "* | url:www." + chunks[i][j] + "*)";
      j++;
    }
    chunkDomainsTemp2 = chunkDomainsTemp.filter(function(item, pos) {
      return chunkDomainsTemp.indexOf(item) === pos;
    });
    chunkDomains.push(chunkDomainsTemp2);
    chunkDomainsTemp = [];
    chunkDomainsTemp2 = [];
    _results.push(i++);
  }
  return _results;
};

$(window).load(function() {
  $("#second").css({
    opacity: 0.2
  });
  return $("#third").css({
    opacity: 0.2
  });
});

$("#parseButton").on("click", function() {
  parseCSV();
  $(this).prop("disabled", true);
  return $(this).css({
    opacity: 0.2
  });
});

$("#divideButton").on("click", function() {
  var i, ii;
  divideResults();
  $(this).prop("disabled", true);
  $("#divideInput").prop("disabled", true);
  $(this).css({
    opacity: 0.2
  });
  $("body").append("<div class='outputContainerWrapper'></div>");
  $(".outputContainerWrapper").append("<div class='outputContainer'></div>");
  $(".outputContainer").append("<div class='step4'>4) Get positions</div><br>");
  i = 0;
  while (i < chunkDomains.length) {
    $(".outputContainer").append($(chunkOutputCode).attr("id", "output" + i));
    ii = i + 1;
    $("#output" + i).children(".outputChunkName").html("#" + ii);
    $("#output" + i).children(".outputChunkDomain").val(chunkDomains[i].join("|"));
    $("#output" + i).children(".outputChunkLinks").val(chunks[i].join("\n"));
    $("#output" + i).children(".chunkResult").attr("id", "selectFile" + i);
    $("#output" + i).children(".chunkResult").attr("name", "file" + i);
    $("#output" + i).children(".inputCode").attr("id", "code" + i);
    i++;
  }
  $(".outputContainer").append("<br><br><a target='_blank' href='pos.php'>insert data here</a><br>");
  return $(".outputContainer").append("<br><button id='filesReady'>done</button><br>");
});

$(document).on("click", "#filesReady", function() {
  var aa, code, j, str1, str2, visualArray;
  $(this).css({
    opacity: 0.2
  });
  $(this).prop("disabled", true);
  code = "";
  str1 = '';
  str2 = '';
  visualArray = [];
  $(".inputCode").each(function(i) {
    var aa, bb, cc, codeArray, dd, hg, j, posArray, resultArray, resultString, urlArray, _results;
    if ($(this).val() !== "") {
      code = $(this).val();
      codeArray = code.split("\n");
      resultArray = [];
      for (j in codeArray) {
        hg = codeArray[j].lastIndexOf("	");
        if (!isNaN(parseInt(codeArray[j][hg + 1]))) {
          resultArray.push(codeArray[j]);
        }
      }
      resultString = resultArray.join("\n");
      posArray = [];
      urlArray = [];
      for (j in resultArray) {
        aa = resultArray[j].split("	");
        bb = aa[1].substring(16);
        cc = bb.indexOf("url:www.");
        dd = bb.substring(0, cc - 4);
        posArray.push(aa[2]);
        urlArray.push(dd);
      }
      visualArray = [];
      _results = [];
      for (j in posArray) {
        _results.push(visualArray.push(posArray[j] + "	" + urlArray[j]));
      }
      return _results;
    }
  });
  visualArray.sort();
  for (j in visualArray) {
    aa = visualArray[j].split("	");
    str1 += aa[0] + "\n";
    str2 += aa[1] + "\n";
  }
  $(".outputContainerWrapper").append("<div class='outputContainer2'></div>");
  $(".outputContainer2").append("<h2>results:</h2>");
  $(".outputContainer2").append("<textarea class='posFinal' disabled>" + str1 + "</textarea>");
  $(".outputContainer2").append("<textarea class='final' disabled>" + str2 + "</textarea>");
  return setTimeout((function() {
    return syncScroll();
  }), 100);
});
